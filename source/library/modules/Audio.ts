export abstract class Audio {

    static context = new AudioContext();

    static playSound(buffer: AudioBuffer) {

        const source = Audio.context.createBufferSource();

        source.buffer = buffer;
        source.connect(Audio.context.destination);

        source.start();
    }
}
