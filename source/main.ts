import { gameConfig } from "./library";

import { Scene } from "./library/modules/Scene";
import { Audio } from "./library/modules/Audio";
import { Assets } from "./library/modules/Assets";
import { Renderer } from "./library/modules/Renderer";

import { Timer } from "./library/utilities/Timer";
import { Vector2 } from "./library/utilities/Vector2";
import { Collisions } from "./library/utilities/Collisions";
import { IdGenerator } from "./library/utilities/IdGenerator";

import { Gui } from "./gui";

const BRICKS_LAYOUT = [
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,2,0,2,0,2,0,2,0,2,0,2,0,0,
    0,3,0,3,4,0,0,3,0,0,4,3,0,3,0,
    0,3,3,3,0,4,3,3,3,4,0,3,3,3,0,
    0,3,3,3,3,3,3,0,3,3,3,3,3,3,0,
    0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,
    0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,
    0,1,4,3,3,3,3,1,3,3,3,3,4,1,0,
    0,4,3,3,3,4,1,4,1,4,3,3,3,4,0,
    0,0,0,3,0,1,1,0,1,1,0,3,0,0,0,
    0,0,0,3,0,3,0,0,0,3,0,3,0,0,0,
    0,3,0,3,0,3,0,0,0,3,0,3,0,3,0,
    0,2,0,2,0,2,0,0,0,2,0,2,0,2,0,
    0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,
    0,3,3,3,3,3,3,0,3,3,3,3,3,3,0,
];

let isBallInPlay = false;
const MOUSE_COORDS = new Vector2();

const MAX_COMBO = 10;
let comboResetAlarmId: number;

const PADDLE_WIDTH = 32;
const PADDLE_HEIGHT = 8;

const BRICK_WIDTH = 16;
const BRICK_HEIGHT = 8;
const BRICK_LAYOUT_COLS = 15;

const PLAYFIELD_WIDTH = BRICK_LAYOUT_COLS * BRICK_WIDTH;
const PLAYFIELD_HEIGHT = Renderer.resolution.y;

const gui = new Gui(
    new Vector2(PLAYFIELD_WIDTH, 0),
    new Vector2(Renderer.resolution.x - PLAYFIELD_WIDTH, Renderer.resolution.y)
);

const EntityLabels = {
    WALL: "wall-label",
    BRICK: "brick-label",
    STEEL: "steel-label",
    FLOOR: "floor-label",
    PADDLE: "paddle-label",
    OBSTACLE: "obstacle-label"
} as const;

const BALL_ENTITY: Entity = {
    id: IdGenerator.generate(),
    pos: new Vector2(
        Renderer.resolution.x / 2,
        PLAYFIELD_HEIGHT - 50
    ),
    vel: new Vector2(),
    size: new Vector2(4, 4),
    sprite: "ball-sprite",
    labels: new Set(),
    context: {}
};

const PADDLE_ENTITY: Entity = {
    id: IdGenerator.generate(),
    pos: new Vector2(
        Renderer.resolution.x / 2 - PADDLE_WIDTH / 2,
        PLAYFIELD_HEIGHT - PADDLE_HEIGHT - 4
    ),
    vel: new Vector2(),
    size: new Vector2(PADDLE_WIDTH, PADDLE_HEIGHT),
    sprite: "paddle-sprite",
    labels: new Set([EntityLabels.OBSTACLE, EntityLabels.PADDLE]),
    context: {}
};

const Walls: Record<"LEFT" | "RIGHT" | "CEILING" | "FLOOR", Entity> = {
    LEFT: {
        id: IdGenerator.generate(),
        pos: new Vector2(),
        vel: new Vector2(),
        size: new Vector2(0, PLAYFIELD_HEIGHT),
        labels: new Set([EntityLabels.OBSTACLE, EntityLabels.WALL]),
        context: {}
    },
    RIGHT: {
        id: IdGenerator.generate(),
        pos: new Vector2(PLAYFIELD_WIDTH, 0),
        vel: new Vector2(),
        size: new Vector2(0, PLAYFIELD_HEIGHT),
        labels: new Set([EntityLabels.OBSTACLE, EntityLabels.WALL]),
        context: {}
    },
    CEILING: {
        id: IdGenerator.generate(),
        pos: new Vector2(),
        vel: new Vector2(),
        size: new Vector2(PLAYFIELD_WIDTH, 0),
        labels: new Set([EntityLabels.OBSTACLE, EntityLabels.WALL]),
        context: {}
    },
    FLOOR: {
        id: IdGenerator.generate(),
        pos: new Vector2(0, PLAYFIELD_HEIGHT),
        vel: new Vector2(),
        size: new Vector2(PLAYFIELD_WIDTH, 0),
        labels: new Set([EntityLabels.OBSTACLE, EntityLabels.FLOOR]),
        context: {}
    }
};

function createBrick(type: number, posX = 0, posY = 0): Entity {
    return {
        id: IdGenerator.generate(),
        pos: new Vector2(posX, posY),
        vel: new Vector2(),
        size: new Vector2(BRICK_WIDTH, BRICK_HEIGHT),
        sprite: `brick-sprite-${type}`,
        labels: new Set([
            EntityLabels.OBSTACLE,
            type !== 4 ? EntityLabels.BRICK : EntityLabels.STEEL
        ]),
        context: {
            hp: type
        }
    };
}

function createBricksLayout() {

    Scene.childrenLabeled.get(EntityLabels.BRICK)?.forEach(entity => {
        Scene.removeChild(entity);
    });

    for(let i = 0; i < BRICKS_LAYOUT.length; i++) {
        if (BRICKS_LAYOUT[i]) {
            Scene.addChild(
                createBrick(
                    BRICKS_LAYOUT[i],
                    i % BRICK_LAYOUT_COLS * BRICK_WIDTH,
                    Math.floor(i / BRICK_LAYOUT_COLS) * BRICK_HEIGHT
                )
            );
        }
    }
}

function handleGameOver() {

    gui.isGameOver = true;
    isBallInPlay = false;

    if (gui.bestScore < gui.$score.value) {
        gui.bestScore = gui.$score.value;
    }
}

function resetBallPosition() {

    BALL_ENTITY.vel.set(0, 0);

    BALL_ENTITY.pos.set(
        PADDLE_ENTITY.pos.x + PADDLE_ENTITY.size.x / 2 - BALL_ENTITY.size.x / 2,
        PADDLE_ENTITY.pos.y - 64
    );
}

function setup() {

    window.addEventListener("click", () => {

        if (gui.isGameOver) {

            createBricksLayout();

            gui.$score.value = 0;
            gui.$combo.value = 1;
            gui.$lives.value = 3;

            gui.isGameOver = false;

            return;
        }

        if (!isBallInPlay) {
            isBallInPlay = true;
            BALL_ENTITY.vel.set(0, 1).mulScalar(200);
        }
    });

    window.addEventListener("mousemove", event => {

        if (gui.isGameOver) return;

        const canvasBox = Renderer.canvas.getBoundingClientRect();

        MOUSE_COORDS.set(
            (event.clientX - canvasBox.left) / (canvasBox.width / Renderer.resolution.x),
            (event.clientY - canvasBox.top) / (canvasBox.height / Renderer.resolution.y)
        );

        PADDLE_ENTITY.pos.x = Math.min(
            Math.max(MOUSE_COORDS.x - PADDLE_ENTITY.size.x / 2, 0),
            PLAYFIELD_WIDTH - PADDLE_ENTITY.size.x
        );

        if (!isBallInPlay) {
            BALL_ENTITY.pos.x =
                PADDLE_ENTITY.pos.x + PADDLE_ENTITY.size.x / 2 - BALL_ENTITY.size.x / 2;
        }
    });

    Scene.addChild(Walls.LEFT);
    Scene.addChild(Walls.RIGHT);
    Scene.addChild(Walls.CEILING);
    Scene.addChild(Walls.FLOOR);

    Scene.addChild(BALL_ENTITY);
    Scene.addChild(PADDLE_ENTITY);

    createBricksLayout();
}

function update(deltaTime: number) {

    Renderer.clear(0, 0, PLAYFIELD_WIDTH, PLAYFIELD_HEIGHT);
    gui.render();

    let collision = Collisions.checkDynamicAabbVsStaticAabbSet(
        BALL_ENTITY,
        Scene.childrenLabeled.get(EntityLabels.OBSTACLE) || new Set(),
        deltaTime
    )[0];

    BALL_ENTITY.pos.add(BALL_ENTITY.vel.clone().mulScalar(deltaTime));

    if (!collision) return;

    BALL_ENTITY.pos.copy(collision.point.clone().sub(BALL_ENTITY.size.clone().divScalar(2)));

    if (collision.normal.x !== 0) BALL_ENTITY.vel.x *= -1;
    if (collision.normal.y !== 0) BALL_ENTITY.vel.y *= -1;

    const { other } = collision;

    if (other.labels.has(EntityLabels.WALL)) {
        Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-wall.wav"));
        return;
    }

    if (other.labels.has(EntityLabels.STEEL)) {
        Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-steel.wav"));
        return;
    }

    if (other.labels.has(EntityLabels.FLOOR)) {

        isBallInPlay = false;
        gui.$lives.value -= 1;

        resetBallPosition();

        if (gui.$lives.value === 0) {
            handleGameOver();
        }

        Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-floor.wav"));
        return;
    }

    if (other.labels.has(EntityLabels.BRICK)) {

        other.context.hp -= 1;

        if (gui.$combo.value < MAX_COMBO) {
            gui.$combo.value += 1;
        }

        if (comboResetAlarmId > 0) {
            Timer.resetAlarm(comboResetAlarmId);
        } else {
            comboResetAlarmId = Timer.setAlarm(() => {
                gui.$combo.value = 1;
                comboResetAlarmId = -1;
            }, 1000);
        }

        if (other.context.hp === 2) {

            gui.$score.value += 5 * gui.$combo.value;
            other.sprite = "brick-sprite-2";
            Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-brick-3.wav"));

            return;
        }

        if (other.context.hp === 1) {

            gui.$score.value += 10 * gui.$combo.value;
            other.sprite = "brick-sprite-1";
            Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-brick-2.wav"));

            return;
        }

        if (other.context.hp <= 0) {

            gui.$score.value += 15 * gui.$combo.value;

            Scene.removeChild(other);
            Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-brick-1.wav"));

            if (Scene.childrenLabeled.get(EntityLabels.BRICK)?.size === 0) {
                handleGameOver();
                resetBallPosition();
            }

            return;
        }
    }

    if (other.labels.has(EntityLabels.PADDLE)) {

        if (collision.normal.y !== -1) return;

        // clamp in range -1 to 1
        const paddleHitPosX =
            (collision.point.x - (other.pos.x + other.size.x / 2)) / (other.size.x / 2);

        let swayAngle = Math.atan2(1, -paddleHitPosX) - Math.PI / 2;
        const ballAngle = Math.atan2(BALL_ENTITY.vel.y, BALL_ENTITY.vel.x) * -1;
        const nextBallAngle = Math.PI - ballAngle + swayAngle;

        if (BALL_ENTITY.vel.x >= 0 && nextBallAngle > Math.PI - Math.PI / 4) {
            swayAngle = Math.PI - Math.PI / 4 - Math.PI + ballAngle;
        } else if (BALL_ENTITY.vel.x < 0 && nextBallAngle < Math.PI / 4) {
            swayAngle = ballAngle + Math.PI / 4 - Math.PI;
        }

        BALL_ENTITY.vel.set(
            Math.cos(swayAngle) * BALL_ENTITY.vel.x - Math.sin(swayAngle) * BALL_ENTITY.vel.y,
            Math.sin(swayAngle) * BALL_ENTITY.vel.x + Math.cos(swayAngle) * BALL_ENTITY.vel.y
        );

        Audio.playSound(<AudioBuffer>Assets.cache.get("assets/sounds/hit-paddle.wav"));
    }
}

gameConfig({
    setup,
    update,
    preload: [
        "assets/sprites/spritesheet.png",
        "assets/sounds/hit-wall.wav",
        "assets/sounds/hit-steel.wav",
        "assets/sounds/hit-floor.wav",
        "assets/sounds/hit-paddle.wav",
        "assets/sounds/hit-brick-1.wav",
        "assets/sounds/hit-brick-2.wav",
        "assets/sounds/hit-brick-3.wav"
    ],
    sprites: {
        "ball-sprite": {
            x: 0,
            y: 0,
            w: 4,
            h: 4,
            atlas: "assets/sprites/spritesheet.png"
        },
        "paddle-sprite": {
            x: 0,
            y: 56,
            w: 32,
            h: 8,
            atlas: "assets/sprites/spritesheet.png"
        },
        "paddle-icon-sprite": {
            x: 32,
            y: 16,
            w: 16,
            h: 8,
            atlas: "assets/sprites/spritesheet.png"
        },
        "brick-sprite-1": {
            x: 0,
            y: 32,
            w: 16,
            h: 8,
            atlas: "assets/sprites/spritesheet.png"
        },
        "brick-sprite-2": {
            x: 0,
            y: 24,
            w: 16,
            h: 8,
            atlas: "assets/sprites/spritesheet.png"
        },
        "brick-sprite-3": {
            x: 0,
            y: 16,
            w: 16,
            h: 8,
            atlas: "assets/sprites/spritesheet.png"
        },
        "brick-sprite-4": {
            x: 16,
            y: 16,
            w: 16,
            h: 8,
            atlas: "assets/sprites/spritesheet.png"
        }
    }
});
