export abstract class ImageLoader {
    static async load(path: string): Promise<GameAsset<HTMLImageElement>> {

        const blob = await (await fetch(path)).blob();
        const image = new Image();

        image.src = URL.createObjectURL(blob);

        return {
            name: path,
            data: image
        };
    }
}
