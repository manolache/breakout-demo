type Observer<T> = (value: T) => void;
type Subscription = () => void;

export class Observable<T = unknown> {

    constructor(
        private innerValue: T
    ) {
        // ...
    }

    private observers = new Set<Observer<T>>();

    get value() {
        return this.innerValue;
    }

    set value(value: T) {

        this.innerValue = value;

        this.observers.forEach(observer => {
            observer(this.innerValue);
        });
    }

    addObserver(observer: Observer<T>): Subscription {
        this.observers.add(observer);
        return () => this.removeObserver(observer);
    }

    removeObserver(observer: Observer<T>) {
        this.observers.delete(observer);
    }
}
