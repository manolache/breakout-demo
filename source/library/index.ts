import { Scene } from "./modules/Scene";
import { Assets } from "./modules/Assets";
import { Renderer } from "./modules/Renderer";

import { Timer } from "./utilities/Timer";
import { Vector2 } from "./utilities/Vector2";

function gameConfig({
    setup,
    update,
    preload,
    sprites
}: GameConfig) {

    let isGamePaused = true;

    Renderer.setup();
    Renderer.renderText(new Vector2(50, 75), "BRAKOUT DEMO", "24px 'Retro Gaming'");
    Renderer.renderText(new Vector2(50, 100), "Click anywhere to start!", "14px 'Retro Gaming'");
    Renderer.renderText(new Vector2(115, Renderer.resolution.y - 15), "Ionut A. Manolache", "8px 'Retro Gaming'");
    Renderer.renderText(new Vector2(150, Renderer.resolution.y - 5), "2023", "8px 'Retro Gaming'");

    async function start() {

        try {

            window.removeEventListener("click", start);

            await Assets.load(preload);

            for (let sprite in sprites) {
                Assets.sprites.set(sprite, sprites[sprite]);
            }

            setup();

            let deltaTimeMs = 0;
            let deltaTimeSec = 0;
            let prevFrameTimeMs = 0;
            let currFrameTimeMs = 0;

            isGamePaused = false;

            function loop() {

                if (isGamePaused) return;

                currFrameTimeMs = performance.now();

                deltaTimeMs = currFrameTimeMs - prevFrameTimeMs;
                deltaTimeSec = deltaTimeMs / 1000;

                prevFrameTimeMs = currFrameTimeMs;

                Timer.update(deltaTimeMs);

                update(deltaTimeSec);

                Scene.children.forEach(entity => {
                    if (
                        entity.sprite &&
                        sprites[entity.sprite] &&
                        Assets.cache.has(sprites[entity.sprite].atlas)
                    ) {
                        Renderer.renderEntity(
                            entity,
                            sprites[entity.sprite],
                            <HTMLImageElement>Assets.cache.get(sprites[entity.sprite].atlas)
                        );
                    }
                });

                requestAnimationFrame(loop);
            }

            window.addEventListener("blur", () => {
                isGamePaused = true;
            });

            window.addEventListener("focus", () => {
                isGamePaused = false;
                prevFrameTimeMs = performance.now();
                requestAnimationFrame(loop);
            });

            requestAnimationFrame(loop);

        } catch(error) {
            console.error(error);
        }
    }

    window.addEventListener("click", start);
}

export {
    gameConfig
}
