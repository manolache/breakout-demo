import { Vector2 } from "../utilities/Vector2";

export abstract class Renderer {

    static canvas = document.createElement("canvas");
    static context = <CanvasRenderingContext2D>Renderer.canvas.getContext("2d");
    static resolution = new Vector2(320, 240);

    static setup() {

        Renderer.canvas.width = Renderer.resolution.x;
        Renderer.canvas.height = Renderer.resolution.y;

        Renderer.canvas.style.setProperty(
            "--ratio",
            `${Renderer.resolution.x / Renderer.resolution.y}`
        );

        Renderer.canvas.style.imageRendering = "pixelated";

        this.canvas.setAttribute("tabindex", "0");
        document.body.prepend(Renderer.canvas);
    }

    static clear(x?: number, y?: number, w?: number, h?: number) {
        Renderer.context.fillStyle = "#111111";
        Renderer.context.fillRect(
            x ?? 0,
            y ?? 0,
            w ?? Renderer.canvas.width,
            h ?? Renderer.canvas.height
        );
    }

    static renderText(
        pos: Vector2,
        text: number | string,
        font: string = "10px sans-serif",
        color: string = "#FFFFFF"
    ) {
        Renderer.context.font = font;
        Renderer.context.fillStyle = color;
        Renderer.context.fillText(String(text), pos.x, pos.y);
    }

    static renderLine(start: Vector2, end: Vector2, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 2;
        Renderer.context.strokeStyle = color;

        Renderer.context.beginPath();
        Renderer.context.moveTo(start.x, start.y);
        Renderer.context.lineTo(end.x, end.y);
        Renderer.context.closePath();
        Renderer.context.stroke();
    }

    static renderCircle(position: Vector2, radius: number, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 2;
        Renderer.context.strokeStyle = color;

        Renderer.context.beginPath();
        Renderer.context.arc(position.x, position.y, radius, 0, 2 * Math.PI);
        Renderer.context.closePath();
        Renderer.context.stroke();
    }

    static renderRectangle(position: Vector2, size: Vector2, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 2;
        Renderer.context.strokeStyle = color;

        Renderer.context.strokeRect(position.x, position.y, size.x, size.y);
    }

    static renderSprite(
        sprite: Sprite,
        atlas: HTMLImageElement,
        position: Vector2,
        size?: Vector2
    ) {

        Renderer.context.imageSmoothingEnabled = false;

        Renderer.context.drawImage(
            atlas,
            sprite.x,
            sprite.y,
            sprite.w,
            sprite.h,
            Math.floor(position.x),
            Math.floor(position.y),
            size?.x ?? sprite.w,
            size?.y ?? sprite.h
        );
    }

    static renderEntity(entity: Entity, sprite: Sprite, atlas: HTMLImageElement) {
        Renderer.renderSprite(sprite, atlas, entity.pos, entity.size);
    }
}
