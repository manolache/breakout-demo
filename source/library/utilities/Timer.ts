import { IdGenerator } from "./IdGenerator";

type Alarm = {
    callback: () => void;
    targetMs: number;
    currentMs: number;
}

const alarms = new Map<number, Alarm>();

export abstract class Timer {

    static update(deltaTimeMs: number) {
        alarms.forEach((alarm, id) => {

            alarm.currentMs += deltaTimeMs;

            if (alarm.currentMs >= alarm.targetMs) {
                alarm.callback();
                alarms.delete(id);
            }
        });
    }

    static setAlarm(callback: () => void, ms: number): number {

        const alarmId = IdGenerator.generate();

        alarms.set(alarmId, {
            callback,
            targetMs: ms,
            currentMs: 0
        });

        return alarmId;
    }

    static resetAlarm(id: number) {
        if (!alarms.has(id)) return;
        (<Alarm>alarms.get(id)).currentMs = 0;
    }

    static cancelAlarm(id: number) {
        alarms.delete(id);
    }
}
