export class Vector2 {

    constructor(
        public x: number = 0,
        public y: number = 0
    ) {
        // ...
    }

    set(x: number, y: number): Vector2 {
        this.x = x;
        this.y = y;
        return this;
    }

    copy(other: Vector2): Vector2 {
        this.x = other.x;
        this.y = other.y;
        return this;
    }

    clone() {
        return new Vector2(this.x, this.y);
    }

    add(other: Vector2): Vector2 {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    addScalar(value: number): Vector2 {
        this.x += value;
        this.y += value;
        return this;
    }

    sub(other: Vector2): Vector2 {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    subScalar(value: number): Vector2 {
        this.x -= value;
        this.y -= value;
        return this;
    }

    mul(other: Vector2): Vector2 {
        this.x *= other.x;
        this.y *= other.y;
        return this;
    }

    mulScalar(value: number): Vector2 {
        this.x *= value;
        this.y *= value;
        return this;
    }

    div(other: Vector2): Vector2 {
        this.x /= other.x;
        this.y /= other.y;
        return this;
    }

    divScalar(value: number): Vector2 {
        this.x /= value;
        this.y /= value;
        return this;
    }
}
