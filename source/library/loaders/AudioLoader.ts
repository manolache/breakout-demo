import { Audio } from "../modules/Audio";

export abstract class AudioLoader {
    static async load(path: string): Promise<GameAsset<AudioBuffer>> {

        const arrayBuffer = await (await fetch(path)).arrayBuffer();
        const audioBuffer = await Audio.context.decodeAudioData(arrayBuffer);

        return {
            name: path,
            data: audioBuffer
        };
    }
}
