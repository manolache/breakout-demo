import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({

  plugins: [
    // ...
  ],

  build: {

    outDir: "output",
    target: ["es2020"],
    emptyOutDir: true,

    rollupOptions: {
      output: {
        entryFileNames: "game.js"
      }
    }
  },

  publicDir: "source/public"
});
