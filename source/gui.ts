import { Assets } from "./library/modules/Assets";
import { Renderer } from "./library/modules/Renderer";

import { Vector2 } from "./library/utilities/Vector2";
import { Observable } from "./library/utilities/Observable";

export class Gui {

    constructor(
        private pos: Vector2,
        private size: Vector2
    ) {

        this.pos.x += 1;

        this.$score.addObserver(() => this.needsUpdate = true);
        this.$combo.addObserver(() => this.needsUpdate = true);
        this.$lives.addObserver(() => this.needsUpdate = true);
    }

    public $score = new Observable(0);
    public $combo = new Observable(1);
    public $lives = new Observable(3);

    public bestScore = 0;
    public isGameOver = false;

    private needsUpdate = true;

    private scorePos = new Vector2(this.pos.x + 5, this.pos.y + 40);
    private scoreLabelPos = new Vector2(this.scorePos.x, this.scorePos.y - 15);

    private comboPos = new Vector2(this.pos.x + 5, this.scorePos.y + 40);
    private comboLabelPos = new Vector2(this.comboPos.x, this.comboPos.y - 15);

    private paddlesPos = new Vector2(this.pos.x + 5, this.comboPos.y + 40);
    private paddlesLabelPos = new Vector2(this.paddlesPos.x, this.paddlesPos.y - 10);

    private bestScorePos = new Vector2(this.pos.x + 5, this.size.y - 20);
    private bestScoreLabelPos = new Vector2(this.bestScorePos.x, this.bestScorePos.y - 15);

    render() {

        if (!this.needsUpdate) return;

        Renderer.clear(this.pos.x, 0, this.size.x, this.size.y);

        Renderer.renderLine(this.pos, new Vector2(this.pos.x, this.size.y), "#FFFFFF");

        Renderer.renderText(this.scoreLabelPos, "SCORE:", "10px 'Retro Gaming'");
        Renderer.renderText(this.scorePos, this.$score.value, "12px 'Retro Gaming'");

        if (this.isGameOver) {
            Renderer.renderText(this.paddlesPos, "GAME OVER", "10px 'Retro Gaming'");
            return;
        }

        Renderer.renderText(this.comboLabelPos, "COMBO:", "10px 'Retro Gaming'");
        Renderer.renderText(this.comboPos, `x${this.$combo.value}`, "12px 'Retro Gaming'");

        Renderer.renderText(this.paddlesLabelPos, "PADDLES:", "10px 'Retro Gaming'");

        for (let i = 0; i < this.$lives.value; i++) {
            Renderer.renderSprite(
                <Sprite>Assets.sprites.get("paddle-icon-sprite"), 
                <HTMLImageElement>Assets.cache.get("assets/sprites/spritesheet.png"), 
                new Vector2(this.paddlesPos.x + i * 22, this.paddlesPos.y)
            );
        }

        Renderer.renderText(this.bestScoreLabelPos, "BEST:", "10px 'Retro Gaming'");
        Renderer.renderText(this.bestScorePos, this.bestScore, "12px 'Retro Gaming'");

        this.needsUpdate = false;
    }
}
