export abstract class Scene {

    static children = new Set<Entity>();
    static childrenLabeled = new Map<string, Set<Entity>>();

    static addChild(child: Entity) {

        if (Scene.children.has(child)) return;

        Scene.children.add(child);

        child.labels.forEach(label => {

            if (!this.childrenLabeled.has(label)) {
                this.childrenLabeled.set(label, new Set());
            }

            this.childrenLabeled.get(label)?.add(child);
        });
    }

    static removeChild(child: Entity) {

        child.labels.forEach(label => {
            this.childrenLabeled.get(label)?.delete(child);
        });

        Scene.children.delete(child);
    }
}