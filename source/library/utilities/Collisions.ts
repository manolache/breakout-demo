import { Vector2 } from "./Vector2";

export abstract class Collisions {

    static checkRayVsStaticAabb(
        raySrc: Vector2,
        rayDir: Vector2,
        targetAabb: StaticAabb
    ): Collision | undefined {

        const tNear = targetAabb.pos.clone().sub(raySrc).div(rayDir);
        const tFar  = targetAabb.pos.clone().add(targetAabb.size).sub(raySrc).div(rayDir);

        if (
            Number.isNaN(tNear.x)   ||
            Number.isNaN(tNear.y)   ||
            Number.isNaN(tFar.x)    ||
            Number.isNaN(tFar.y)
        ) return;


        if (tNear.x > tFar.x) {
            let temp    = tNear.x;
            tNear.x     = tFar.x;
            tFar.x      = temp;
        }

        if (tNear.y > tFar.y) {
            let temp    = tNear.y;
            tNear.y     = tFar.y;
            tFar.y      = temp;
        }

        if (tNear.x > tFar.y || tNear.y > tFar.x) return;

        const tHitNear = Math.max(tNear.x, tNear.y);
        const tHitFar = Math.min(tFar.x, tFar.y);

        if (tHitFar < 0 || !(tHitNear < 1)) return;

        const point = raySrc.clone().add(rayDir.clone().mulScalar(tHitNear));
        const normal = new Vector2();

        if (tNear.x >= tNear.y) {
            rayDir.x < 0 ? normal.set(1, 0) : normal.set(-1, 0);
        } else {
            rayDir.y < 0 ? normal.set(0, 1) : normal.set(0, -1);
        }

        return {
            time: tHitNear,
            other: targetAabb as Entity,
            point,
            normal,
        };
    }

    static checkDynamicAabbVsStaticAabb(
        source: DynamicAabb,
        target: StaticAabb,
        deltaTime: number
    ): Collision | undefined {

        const expandedTarget: StaticAabb = {
            pos: target.pos.clone().sub(source.size.clone().divScalar(2)),
            size: target.size.clone().add(source.size)
        };

        const collision = Collisions.checkRayVsStaticAabb(
            source.pos.clone().add(source.size.clone().divScalar(2)),
            source.vel.clone().mulScalar(deltaTime),
            expandedTarget
        );

        if (collision) {

            if (collision.time < 0 || collision.time > 1) return;

            collision.other = <Entity>target;
            return collision;
        }

        return;
    }

    static checkDynamicAabbVsStaticAabbSet(
        source: DynamicAabb,
        targets: Set<StaticAabb>,
        deltaTime: number
    ): Collision[] {

        const collisions: Collision[] = [];

        targets.forEach(target => {

            if ((<Entity>target).id === (<Entity>source).id) return;

            const collision = this.checkDynamicAabbVsStaticAabb(source, target, deltaTime);
            if (collision) collisions.push(collision);
        });

        collisions.sort((a, b) => a.time - b.time);

        return collisions;
    }
}
