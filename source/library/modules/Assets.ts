import { AudioLoader } from "../loaders/AudioLoader";
import { ImageLoader } from "../loaders/ImageLoader";

const FILE_TYPE_REGEX = /\.[^.]+$/i;

const FileTypes = {
    MP3: ".mp3",
    WAV: ".wav",
    PNG: ".png"
} as const;

export abstract class Assets {

    static cache = new Map<string, unknown>();
    static sprites = new Map<string, Sprite>();

    static async load(paths: string[]): Promise<void> {

        const promises: Promise<any>[] = [];

        paths.forEach(path => {

            const fileType: string = <string>FILE_TYPE_REGEX.exec(path)?.[0];

            switch (fileType) {

                case FileTypes.MP3:
                case FileTypes.WAV:
                    promises.push(AudioLoader.load(path));
                    break;

                case FileTypes.PNG:
                    promises.push(ImageLoader.load(path));
                    break;

                default:
                    promises.push(Promise.reject(`Failed to load asset from "${path}", type "${fileType}" is not valid.`));
            }
        });

        try {

            (await Promise.all(promises)).forEach(asset => {
                Assets.cache.set(asset.name, asset.data);
            });

            return Promise.resolve();

        } catch(error) {
            return Promise.reject(error);
        }
    }
}
