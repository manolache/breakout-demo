type Vector2 = import("./utilities/Vector2").Vector2;

type GameConfig = {
    setup: () => void;
    update: (deltaTime: number) => void;
    preload: string[];
    sprites: Record<string, Sprite>;
};

type GameAsset<T = unknown> = {
    name: string;
    data: T;
};

type Sprite = {
    x: number;
    y: number;
    w: number;
    h: number;
    atlas: string;
};

type Entity = {
    id: number;
    pos: Vector2;
    vel: Vector2;
    size: Vector2;
    sprite?: string;
    labels: Set<string>;
    context: Record<string, any>;
};

type Collision = {
    time: number;
    other: Entity;
    point: Vector2;
    normal: Vector2;
};

type StaticAabb = {
    pos: Vector2;
    size: Vector2;
};

type DynamicAabb = {
    vel: Vector2;
} & StaticAabb;
